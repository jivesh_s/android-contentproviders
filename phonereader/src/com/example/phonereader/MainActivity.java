package com.example.phonereader;



import java.util.ArrayList;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.RawContacts.Data;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.OperationApplicationException;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

@SuppressLint({ "NewApi", "ShowToast" }) @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH) public class MainActivity extends Activity {
    
    
	String [] details = new String[10];
	
	 private String name1;
	 private String phone1;
	 private String email1;
	 private String org;
	 EditText organization;
	 Display image2;
	 EditText name;
	 EditText phone;
	 EditText email;
	 Button enter;
	 Button choosefile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        enter = (Button)findViewById(R.id.enter);
        enter.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				name = (EditText)findViewById(R.id.name);
				phone = (EditText)findViewById(R.id.phone);
				email = (EditText)findViewById(R.id.email);
				organization = (EditText)findViewById(R.id.organization); 
			
				name1 = name.getText().toString();
				phone1 = phone.getText().toString();
				email1 = email.getText().toString();
				org = organization.getText().toString();


		        ArrayList<ContentProviderOperation> cp = new ArrayList<ContentProviderOperation>();
		        int rawContactInsertIndex = cp.size();

		        cp.add(ContentProviderOperation.newInsert(RawContacts.CONTENT_URI)
		           .withValue(RawContacts.ACCOUNT_TYPE, null)
		           .withValue(RawContacts.ACCOUNT_NAME,null )
		           .build());
		        cp.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
		           .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
		           .withValue(Data.MIMETYPE,Phone.CONTENT_ITEM_TYPE)
		           .withValue(Phone.NUMBER, phone1)
		           .build());
		        cp.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
		           .withValueBackReference(Data.RAW_CONTACT_ID, rawContactInsertIndex)
		           .withValue(Data.MIMETYPE,StructuredName.CONTENT_ITEM_TYPE)
		           .withValue(StructuredName.DISPLAY_NAME, name1)
		           .build()); 
		        
		        cp.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
				           .withValueBackReference(Data.RAW_CONTACT_ID, rawContactInsertIndex)
				           .withValue(Data.MIMETYPE,Email.CONTENT_ITEM_TYPE)
				           .withValue(Email.ADDRESS, email1)
				           .build()); 
		        cp.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
				           .withValueBackReference(Data.RAW_CONTACT_ID, rawContactInsertIndex)
				           .withValue(Data.MIMETYPE,Organization.CONTENT_ITEM_TYPE)
				           .withValue(Organization.COMPANY,org )
				           .build()); 
		        try {
					ContentProviderResult[] res = getContentResolver().applyBatch(ContactsContract.AUTHORITY, cp);
				} catch (RemoteException e) {
				
					e.printStackTrace();
				} catch (OperationApplicationException e) {
					
					e.printStackTrace();
				}
		        
		        name.setText("");
		        phone.setText("");
		        email.setText("");
		        organization.setText("");
		        
		        Toast Message = Toast.makeText(getApplicationContext(), "Contact Added To Your Contact List", Toast.LENGTH_LONG);
		        Message.show();
			}
		});
    }
}
